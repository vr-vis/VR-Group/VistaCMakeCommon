include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VSAMPLERATE_FOUND )
	vista_find_package_root( samplerate include/samplerate.h NAMES libsamplerate samplerate )	

	if( SAMPLERATE_ROOT_DIR )
		set( SAMPLERATE_INCLUDE_DIRS "${SAMPLERATE_ROOT_DIR}/include" )		
		set( SAMPLERATE_LIBRARY_DIRS "${SAMPLERATE_ROOT_DIR}/lib" "${SAMPLERATE_ROOT_DIR}/bin" )	
		if( WIN32 )
			set( SAMPLERATE_LIBRARIES "libsamplerate-0" )
		else( )
			set( SAMPLERATE_LIBRARIES "samplerate" )
		endif( )
	endif( )
endif( )

find_package_handle_standard_args( VSAMPLERATE "samplerate could not be found" SAMPLERATE_ROOT_DIR )
