include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VOPENCASCADE_FOUND )
    vista_find_package_root( OpenCASCADE inc/OSD.hxx )
    if( OPENCASCADE_ROOT_DIR )
        set( OPENCASCADE_INCLUDE_DIRS "${OPENCASCADE_ROOT_DIR}/inc" )
        set( OPENCASCADE_LIBRARY_DIRS "${OPENCASCADE_ROOT_DIR}/win64/vc12/lib" "${OPENCASCADE_ROOT_DIR}/win64/vc12/bin" )
		set( OPENCASCADE_LIBRARIES optimized TKBin )
    endif( OPENCASCADE_ROOT_DIR )
endif( NOT VOPENCASCADE_FOUND )

find_package_handle_standard_args( VOpenCASCADE "OpenCASCADE could not be found" OPENCASCADE_ROOT_DIR )