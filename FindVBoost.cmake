

include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VBOOST_FOUND )
	if( NOT BOOST_ROOT )
		set( Boost_NO_BOOST_CMAKE ON )
		vista_find_package_root( Boost "boost/any.hpp" NAMES BOOST boost NO_CACHE )
		if( NOT BOOST_ROOT_DIR )
			vista_find_package_root( Boost "include/boost/any.hpp" NAMES BOOST boost NO_CACHE )
		endif()

		if( BOOST_ROOT_DIR )
			set( BOOST_ROOT "${BOOST_ROOT_DIR}" )			
		endif()
	endif()
	
	if( WIN32 )
		list( APPEND BOOST_LIBRARY_DIRS "${BOOST_ROOT_DIR}/lib64-msvc-12.0" )
		list( APPEND BOOST_LIBRARY_DIRS "${BOOST_ROOT_DIR}/lib" )
		message( STATUS "BOOST extra lib dirs:  ${BOOST_LIBRARY_DIRS}" )
	endif( )
	
	set( Boost_VERSION ) # this was set by vista_find_package_root, so we need to reset it
	vista_find_original_package( VBoost )
	
	set( BOOST_INCLUDE_DIRS ${Boost_INCLUDE_DIRS} )
	set( BOOST_LIBRARY_DIRS ${Boost_LIBRARY_DIRS} )
	set( BOOST_LIBRARIES ${Boost_LIBRARIES} )

endif()

find_package_handle_standard_args( VBOOST "BOOST could not be found at " BOOST_ROOT_DIR )





