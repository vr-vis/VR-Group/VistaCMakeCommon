include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VMATHGL_FOUND )
	vista_find_package_root( MATHGL "include/mgl2/mgl.h" NAMES mathgl )	

	if( MATHGL_ROOT_DIR )
		set( MATHGL_INCLUDE_DIRS "${MATHGL_ROOT_DIR}/include" )		
		set( MATHGL_LIBRARY_DIRS "${MATHGL_ROOT_DIR}/lib" "${MATHGL_ROOT_DIR}/bin" )
		set( MATHGL_LIBRARIES mgl )
	endif( )
endif( )

find_package_handle_standard_args( VMATHGL "MATHGL could not be found" MATHGL_ROOT_DIR )
