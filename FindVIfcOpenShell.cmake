include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VIFCOPENSHELL_FOUND )

	vista_find_package_root( IFCOPENSHELL "include/ifcparse/Ifc2x3.h" NAMES IfcOpenShell )	

	if( IFCOPENSHELL_ROOT_DIR )
		set( IFCOPENSHELL_INCLUDE_DIRS "${IFCOPENSHELL_ROOT_DIR}/include" )		
		set( IFCOPENSHELL_LIBRARY_DIRS "${IFCOPENSHELL_ROOT_DIR}/lib" "${IFCOPENSHELL_ROOT_DIR}/bin" )	
		set( IFCOPENSHELL_LIBRARIES optimized ifcparse debug ifcparsed optimized ifcgeom debug ifcgeomd )
	endif( IFCOPENSHELL_ROOT_DIR )
	
endif( NOT VIFCOPENSHELL_FOUND )

find_package_handle_standard_args( VIFCOPENSHELL "IFCOPENSHELL could not be found" IFCOPENSHELL_ROOT_DIR )
