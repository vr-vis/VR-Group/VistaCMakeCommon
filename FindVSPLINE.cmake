include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

if( NOT VSPLINE_FOUND )
	vista_find_package_root( SPLINE "include/spline.h" )

	if( SPLINE_ROOT_DIR )
		set( SPLINE_INCLUDE_DIRS "${SPLINE_ROOT_DIR}/include" )
		set( SPLINE_LIBRARY_DIRS "${SPLINE_ROOT_DIR}/lib" )
		set( SPLINE_LIBRARIES optimized SPLINE debug SPLINED )
	endif( )
endif( )

find_package_handle_standard_args( VSPLINE "SPLINE could not be found" SPLINE_ROOT_DIR )
