include( FindPackageHandleStandardArgs )
include( VistaFindUtils )

set( VA_WITH_SOURCE_DEV_ENV FALSE )

vista_find_package( VABase QUIET )
vista_find_package( VANet QUIET )
vista_find_package( VACore QUIET )

# Include VA from source if included in cmake dev env
if( VABASE_FOUND AND VANET_FOUND )
	message(STATUS "Use VirtualAcoustics from source")
	set( VA_WITH_SOURCE_DEV_ENV TRUE )
	vista_use_package( VABase REQUIRED FIND_DEPENDENCIES )
	vista_use_package( VANet REQUIRED FIND_DEPENDENCIES )
	if( VACORE_FOUND )
		vista_use_package( VACore REQUIRED FIND_DEPENDENCIES )
	endif( )
endif( )

# Include as an ExternalLibs package, if dev env package not available
if( NOT VA_WITH_SOURCE_DEV_ENV )

	if( NOT VVIRTUALACOUSTICS_FOUND )
		set( VABASE_FOUND FALSE )
		
		vista_find_package_root( VirtualAcoustics "include/VACore.h" )
		message(STATUS ${VIRTUALACOUSTICS_ROOT_DIR})
		if( VIRTUALACOUSTICS_ROOT_DIR )
		
			set( VIRTUALACOUSTICS_INCLUDE_DIRS "${VIRTUALACOUSTICS_ROOT_DIR}/include" )	
			set( VIRTUALACOUSTICS_LIBRARY_DIRS "${VIRTUALACOUSTICS_ROOT_DIR}/lib" )

			if( VIRTUALACOUSTICS_LIBRARY_DIRS )
				set( VABASE_FOUND TRUE )
				set( VIRTUALACOUSTICS_LIBRARIES optimized VABase debug VABaseD )			
						
				# determine packages to include
				if( EXISTS "${VIRTUALACOUSTICS_ROOT_DIR}/include/VANetServer.h" )
					set( VVirtualAcoustics_VANet_FOUND TRUE )
					set( VirtualAcoustics_VANet_FOUND TRUE )
					
					list( FIND VirtualAcoustics_FIND_COMPONENTS VANet VANET_REQUESTED )
					if( VANET_REQUESTED OR NOT VirtualAcoustics_FIND_COMPONENTS )
						list( APPEND VIRTUALACOUSTICS_LIBRARIES optimized VANet debug VANetD )
					endif()
				else()
					set( VVirtualAcoustics_VANet_FOUND FALSE )
					set( VirtualAcoustics_VANet_FOUND FALSE )
				endif()
				
				if( EXISTS "${VIRTUALACOUSTICS_ROOT_DIR}/include/VACoreFactory.h" )
					set( VVirtualAcoustics_VACore_FOUND TRUE )
					set( VirtualAcoustics_VACore_FOUND TRUE )
					
					list( FIND VirtualAcoustics_FIND_COMPONENTS VACore VACORE_REQUESTED )
					if( VACORE_REQUESTED OR NOT VirtualAcoustics_FIND_COMPONENTS )
						list( APPEND VIRTUALACOUSTICS_LIBRARIES optimized VACore debug VACoreD )
					endif()
				else()
					set( VVirtualAcoustics_VACore_FOUND FALSE )
					set( VirtualAcoustics_VACore_FOUND FALSE )
				endif()
				
				list( FIND VirtualAcoustics_FIND_COMPONENTS 3rdParty 3RDPARTY_REQUESTED )
				if( 3RDPARTY_REQUESTED )
					list( APPEND VIRTUALACOUSTICS_LIBRARY_DIRS 	"${VIRTUALACOUSTICS_ROOT_DIR}/3rdParty/fftw3/bin/${POSTFIX}" 
																"${VIRTUALACOUSTICS_ROOT_DIR}/3rdParty/ipp/bin/${POSTFIX}"
																"${VIRTUALACOUSTICS_ROOT_DIR}/3rdParty/tbb/bin/${POSTFIX}" 
																"${VIRTUALACOUSTICS_ROOT_DIR}/bin")
					#"   )
					set( VVirtualAcoustics_3rdParty_FOUND TRUE )
				endif()
			endif()
		endif()
	endif()

	find_package_handle_standard_args( VVirtualAcoustics REQUIRED_VARS VIRTUALACOUSTICS_LIBRARY_DIRS VIRTUALACOUSTICS_INCLUDE_DIRS HANDLE_COMPONENTS 
										)#FAIL_MESSAGE "VirtualAcoustics with requested components ${VVirtualAcoustics_FIND_COMPONENTS} could not be found (VABase found: ${VABASE_FOUND} | VANet found: ${VirtualAcoustics_VANet_FOUND} | VACore found: ${VirtualAcoustics_VACore_FOUND} )" )

endif( )
